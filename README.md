# OpenML dataset: Forest-Surfaces-in-Romania-1990-2019

https://www.openml.org/d/43782

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
We publish the data to clarify the real evolution of forest area in post-communist Romania. The data is from the National Statistics Institute of Romania, so these are the official reported data. 
Content
The data contains the values of Forest surfaces in Romania, from 1990 to 2019, as a total, per region and county per type of forest (Woods, Softwood, Hardwood, Other).
Romania has 42 counties (in Romanian: Judete), including the Capital city municipality, Bucharest (in Romanian: Bucuresti)
Acknowledgements
The source of the data is the Romanian National Statistics Institute.
Inspiration
Use this data to clarify if the forestry surfaces in Romania were declining since 1990 or are increasing. See where the surfaces increased or decreased.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43782) of an [OpenML dataset](https://www.openml.org/d/43782). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43782/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43782/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43782/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

